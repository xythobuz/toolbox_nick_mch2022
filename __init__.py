import display
import mch22
import nvs
import buttons
import sys
import neopixel
import machine
import time
import audio

np = neopixel.NeoPixel(machine.Pin(5), 5)

APP_PATH = "/".join(__file__.split("/")[:-1])

# Add Application path to sys path to import modules as if they were root modules
sys.path.append(APP_PATH)


def btn_home(pressed):
    if pressed:
        mch22.exit_python()

def btn_a(pressed):
	if pressed:
	    channel_id = audio.play("%s/%s" % (APP_PATH, "a.mp3"), volume=100)

def btn_b(pressed):
	if pressed:
	    channel_id = audio.play("%s/%s" % (APP_PATH, "b.mp3"), volume=100)

buttons.attach(buttons.BTN_HOME, btn_home)
buttons.attach(buttons.BTN_A, btn_a)
buttons.attach(buttons.BTN_B, btn_b)

font = "Roboto_Regular18"
font2 = "Roboto_Regular22"

nickname = nvs.nvs_getstr('owner','nickname')
label = "MCH2022"

text_width = display.getTextWidth(nickname, font)
text_width2 = display.getTextWidth(label, font2)

display_size = display.size()
png_width = 155

offset = ( display_size[0] - png_width ) // 2
text_offset = ( display_size[0] - text_width ) // 2
text_offset2 = ( display_size[0] - text_width2 ) // 2

display.drawFill(0x424D50)

display.drawPng(offset, 0, "%s/%s" % (APP_PATH, "toolbox.png"))

display.drawText(text_offset, 190, nickname, 0x9CFF08, font)

#display.drawText(text_offset2, 200, label, 0x000000, font2)

display.flush()

def setleds(r,g,b):
 	for led in range(5):
		np[led] = (r, g, b)
	np.write()

i=0
direction=0
while True:
	setleds(0,0,i)
	time.sleep(0.01)
	if direction == 0:
		i+=1
		if i>150:
			direction=1
	else:
		i-=1
		if i <= 0:
			direction=0
